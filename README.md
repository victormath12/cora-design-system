# Cora Design system

![Image of Component](./images/cora-ds.png)

Este documento representa um conceito de arquitetura e uso de um Design System em `React` desenvolvido pelo banco Cora.

1. [Getting Started](https://gitlab.com/victormath12/cora-design-system/blob/master/getting-started.md)
2. [Typography](https://gitlab.com/victormath12/cora-design-system/blob/master/typography.md)
3. [Button](https://gitlab.com/victormath12/cora-design-system/blob/master/button.md)
4. [Stepper](https://gitlab.com/victormath12/cora-design-system/blob/master/stepper.md)
5. [Input](https://gitlab.com/victormath12/cora-design-system/blob/master/input.md)
6. [Illustration](https://gitlab.com/victormath12/cora-design-system/blob/master/Illustration.md)
7. [Brand](https://gitlab.com/victormath12/cora-design-system/blob/master/brand.md)