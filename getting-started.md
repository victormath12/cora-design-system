## Como Instalar
Para Instalar o Cora Design System, no seu terminal, execute:

```
yarn add @cora/ds
// ou
npm install @cora/ds
```

## Como Usar
Para usar os componentes é necessario utilizar o `ThemeProvider` do Cora para que os componentes possam receber as especificações de design-tokens.

```
import { ThemeProvider, Button } from '@cora/ds';
const App = () => (
  <ThemeProvider>
    <Button>Find an activity</Button>
  </ThemeProvider>
);
```