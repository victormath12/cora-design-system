## Arquiteturas Front

```
Cite as arquiteturas de frontend com as quais tenha trabalhado. Como você divide a suite de testes em cada uma delas?
```

Já trabalhei com alguns tipos de arquiteturas, algumas se diferem quando estamos atuando com frameworks ou libraries front-end diferentes, mas em geral são:

### Arquitetura de Folder

- Angular com arquitetura [Folders-by-Feature](https://angular.io/guide/styleguide#folders-by-feature-structure).

- React com arquitetura [Folders-by-Feature](https://reactjs.org/docs/faq-structure.html).

### Arquitetura de Front

- Server-Side Rendening com React (Razzle, Next.js)

- Backend-for-Front-End (BFF) com Node.js e uns pilotos usando GraphQL.

### Arquitetura de Repo

- Multirepo: Cada package ou módulo é dividido por repositório.

- Monorepo: Vários packages em um mesmo repositório (Usando Lerna).

### Test Suit

Como sempre segui o padrão Folders-by-Feature sempre criei a suite de teste unitário junto ao component ou module:

```
profile/
  index.js
  Profile.js
  Profile.css
  Profile.test.js
```

Somente crio testes de integração (protactor, cypress) apartados da feature, pois entendo que os testes de integração fazem relação a aplicação como um todo e não ao componente.
