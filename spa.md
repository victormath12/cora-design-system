## Quando usar um SPA

```
Em quais cenários você DEVE usar um SPA? E quais você NÃO DEVE?
```

Hoje SPA é algo muito comum em desenvolvimento web, no qual todos os frameworks web modernos se baseiam, ou seja a curva de aprendizado é bem menor hoje em dia.

Eu acredito que SPA é uma boa abordagem quando lidamos com aplicações que necessitam de rendenização mais ágil e uma experiência mais fluída para o usuário pois o mesmo não terá que ver a página recarregando completamente inúmeras vezes.

Um dos problemas do SPA é o SEO, pois o conteúdo da página é carregado dinamicamente enquanto o usuario navega, algo que pode ser resolvido aplicando um SSR, o que leva a outro problema que é o overloading de módulos e o re-rendening, no qual se o time de front não estiver atento na forma que controlam os seus componentes e arquitetura de front (usando corretamente o state, aplicando técnicas de lazy-loading, etc) o front-end poderá se tornar lento e prejudicar a experiência do usuário.

Eu acredito que alguns cenários mais simples como landing-pages, email marketing, hotsites, em muitos casos não precisam ser pensados como um SPA, no entanto não posso dizer quando DEVEMOS ou NÃO DEVEMOS usar pois tudo depende da finalidade, do perfil do time, da arquitetura, do cliente e etc.

