## Illustration

![Image of Component](./images/man-walking.jpg)

```
import { Illustration } from '@cora/ds';

<Illustration 
  name="man-walking"
  alt="Homem andando tranquilo pois tem uma conta no banco Cora"
  width="100%" 
/>
```

### Props

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| name     | string |          | true     |
| alt      | string | ""       | false    |
| width    | string | ""       | false    |
| height   | string | ""       | false    |
| style    | object | {}       | false    |