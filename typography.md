## Typography

![Image of Component](./images/typography.png)

```
import { Typography } from '@cora/ds';

<Typography variant="h2">Dividindo o Peso</Typography>

<Typography variant="h2">Dividindo a jornada</Typography>

<Typography variant="p">Na Cora não tem burocracia, o seu tempo é para cuidar do seu negócio.</Typography>
```

### Props

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| children | node   | null     | false    |
| variant  | string | h1       | false    |
| align    | string: 'left', 'center', 'right', 'justify'   | left    | false    |
| color    | string: 'default', 'error', 'primary', 'secondary', | default     | false    |