## Button Component

![Image of Component](./images/button.png)

### Default

```
import { Button } from '@cora/ds';

<Button>Começar</Button>
```

![Image of Component](./images/button-disabled.png)

```
import { Button } from '@cora/ds';

<Button disabled icon="arrow-right">
  Próximo
</Button>
```


### Outline

![Image of Component](./images/button-outline.png)

```
import { Button } from '@cora/ds';

<Button.Outline>Login</Button.Outline>
```

### Props

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| children | node   | null     | false    |
| disabled | bool   | false    | false    |
| full     | bool   | false    | false    |
| icon     | string | null     | false    |
| onClick  | func   | () => {} | false    |