## Stepper Component

![Image of Component](./images/stepper.png)

```
import { Stepper } from '@cora/ds';

<Stepper activeStep={1}>

  <Stepper.Step id={1}>
    step 1
  </Stepper.Step>

  <Stepper.Step id={2}>
    step 2
  </Stepper.Step>

</Stepper>
```

### Props

#### Stepper

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| children | node   | `<Stepper.Step>` | false    |
| activeStep | number   |     | true    |


#### Stepper.Step

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| children | node   | null     | false    |
| id       | number |          | true     |