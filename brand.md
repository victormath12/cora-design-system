## Brand

![Image of Component](./images/Cora.png)

```
import { Brand } from '@cora/ds';

<Brand alt="banco Cora" width="200px" />
```


![Image of Component](./images/cora-small.jpeg)

```
import { Brand } from '@cora/ds';

<Brand small alt="banco Cora" width="400px" />
```

### Props

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| alt      | string | ""       | false    |
| small    | bool   | false    | false    |
| width    | string | ""       | false    |
| height   | string | ""       | false    |
| style    | object | {}       | false    |