## Default Input Component

![Image of Component](./images/input.png)

### Default

```
import { Input } from '@cora/ds';

<Input 
  label="Qual seu nome completo"
  name="full-name"
  autofocus
  required
/>
```

### Props

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| type     | string |          | true     |
| label    | string |          | true     |
| name     | string |          | true     |
| placeholder  | string   | null    | false    |
| value  | string   | null    | false    |
| autoFocus  | bool   | false | false    |
| disabled   | bool   | false | false    |
| error      | string | ""    | false    |
| required   | bool   | false | false    |
| onChange   | func   | () => {} | false    |



--------------



## Input Password Component

![Image of Component](./images/input-password.png)

```
import { Input } from '@cora/ds';

<Input.Password 
  label="Crie sua senha de 6 dígitos para acessar a sua conta Cora"
  name="password"
  visibilityIcon
  autofocus
  required
  onChange={value => validate(value)}
/>
```

![Image of Component](./images/input-password-valid.png)

```
import { Input } from '@cora/ds';

<Input.Password 
  label="Crie sua senha de 6 dígitos para acessar a sua conta Cora"
  name="password"
  valid
  visibilityIcon
  autofocus
  required
  onChange={value => validate(value)}
/>
```

### Props

| Prop     | Type   | Default  | Required |
|----------|--------|----------|----------|
| label    | string |          | true     |
| name     | string |          | true     |
| visibilityIcon  | bool   |  false   | false    |
| value    | string | null     | false   |
| valid    | bool   | false    | false   |
| autoFocus  | bool   | false | false    |
| disabled   | bool   | false | false    |
| error      | string | ""    | false    |
| required   | bool   | false | false    |
| onChange   | func   | () => {} | false    |