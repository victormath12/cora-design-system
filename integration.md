## Integração Backend & Frontend

````
Imagine o seguinte cenário, a aplicação frontend está hospedado no endereço 'https://meuapp.com' e o backend/api está em 'https://minhaapi.com'. Quais problemas podem acontecer? E como você resolveria se você pudesse alterar qualquer coisa, inclusive o domínio das aplicações e código do backend?
````

Numa arquitetura distribuída é um bom caminho separar o front-end do back-end para que possam ser construidos e deployados de forma paralela e independente. Por exemplo, o front pode estar em um repo no github e ser publicado em um AWS S3 e o microservico em outro repo e ser publicado no AWS EC3 ou Lambda.

Com base nisso, se tivermos servidores diferentes e domínio diferentes para a aplicação frontend e os microserviços provavelmente teremos alguns problemas em união,tratamento, cruzamento e enriquecimento de informações de diversos domínios e microserviços para servir o front-end.

Um exemplo simples disso é lidar com o CORS entre back e front (que pode ser resolvido facilmente através de uma configuração na camada de backend permitindo o domínio da aplicação acessar o serviço).

Também mais complexo como quando temos serviços que nos retornam um buffer de dados muito grandes sendo que precisamos apenas de um simples dado (ainda mais se considerarmos que muitas pessoas utilizam celulares com 3G) ou se precisamos enriquecer algum dado antes de servir o front, para isso é uma boa abordagem seguir uma arquitetura de BFF.